# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## 2019-04-12 [Initial Commit]

### Added
- Search for Albums using Artist Name.
- Internal Storage for Albums, Save/Remove.
- Display for Albums details.
- Awesome Unicorn logo. 