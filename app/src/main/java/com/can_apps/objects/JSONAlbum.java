package com.can_apps.objects;

import org.json.JSONObject;

public class JSONAlbum extends JSONObject {
    private String name;
    private String artist;
    private String tracks;
    private String image;
    private boolean favorite;
    
    public JSONAlbum(JSONObject album) {
        this.name = album.optString("name", "");
        this.artist = album.optString("artist", "");
        this.tracks = album.optString("tracks", "");
        this.image = album.optString("image", "");
        this.favorite = album.optBoolean("favorite", false);
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getArtist() {
        return artist;
    }
    
    public String getTracks() {
        return tracks;
    }
    
    public void setTracks(String tracks) {
        this.tracks = tracks;
    }
    
    public String getImage() {
        return image;
    }
    
    public boolean isFavorite() {
        return favorite;
    }
    
    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}
