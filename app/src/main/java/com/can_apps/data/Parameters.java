package com.can_apps.data;
public class Parameters {
    
    public static final String METHOD = "method";
    public static final String METHOD_ALBUM_GET_INFO = "album.getinfo";
    public static final String METHOD_ARTIST_GET_TOPALBUMS = "artist.gettopalbums";
    
    public static final String FORMAT = "format";
    public static final String FORMAT_JSON = "json";
    
    public static final String API_KEY = "api_key";
    
    public static final String ARTIST = "artist";
    public static final String ALBUM = "album";
    public static final String PAGE = "page";
}
