package com.can_apps.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;

import com.can_apps.Keys;
import com.can_apps.objects.JSONAlbum;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Repository is a singleton class responsible for the management and cache from the data.
 * It uses API Call and Internal Storage access.
 */
public class Repository extends ContextWrapper {
    private WebService webService;
    private JSONAlbum selectedAlbum;
    
    private ArrayList<JSONAlbum> searchArrayList = new ArrayList<>();
    
    private String filename = "myAlbumsFile";
    
    private volatile static Repository uniqueInstance;
    
    private Repository(Context base) {
        super(base);
        webService = WebService.retrofit.create(WebService.class);
    }
    
    public static Repository getInstance(Context base) {
        if (uniqueInstance == null) {
            synchronized (Repository.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new Repository(base);
                }
            }
        }
        return uniqueInstance;
    }
    
    /**
     * Method responsible for save and remove the albums from the Internal Storage
     *
     * @param selectedAlbum Album to save/remove
     * @param save boolean responsible to indicate action True/False Save/Remove
     * @return return boolean indication if the action was success full
     */
    public boolean saveAlbum(JSONAlbum selectedAlbum, boolean save) {
        JSONArray arrayAlbums = getAlbumsFileArray();
        
        JSONObject object = new JSONObject();
        try {
            object.put("name", selectedAlbum.getName());
            object.put("artist", selectedAlbum.getArtist());
            object.put("image", selectedAlbum.getImage());
            object.put("tracks", selectedAlbum.getTracks());
            object.put("favorite", true);
        }
        catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
        
        if (save) {
            arrayAlbums.put(object);
        }
        else {
            for (int i = 0; i < arrayAlbums.length(); i++) {
                try {
                    if (arrayAlbums.getJSONObject(i).getString("name")
                            .equals(object.getString("name"))
                            && arrayAlbums.getJSONObject(i).getString("artist")
                            .equals(object.getString("artist"))) {
                        arrayAlbums.remove(i);
                        break;
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        String fileContent = arrayAlbums.toString();
        (new InternalStorage(this)).write(filename, fileContent.getBytes());
        
        return save;
    }
    
    /**
     * Method responsible to get the Albums from local storage
     *
     * @return Live data array with JSON Albums
     */
    public LiveData<ArrayList<?>> getSavedAlbums() {
        final MutableLiveData<ArrayList<?>> data = new MutableLiveData<>();
        ArrayList<JSONAlbum> albumArrayList = new ArrayList<>();
        
        JSONArray arrayAlbums = getAlbumsFileArray();
        
        try {
            for (int i = 0; i < arrayAlbums.length(); i++) {
                albumArrayList.add(new JSONAlbum(arrayAlbums.getJSONObject(i)));
            }
            data.setValue(albumArrayList);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        
        return data;
    }
    
    // private method to read the albums from Internal Storage
    private JSONArray getAlbumsFileArray() {
        String albumsString = (new InternalStorage(this)).read(filename);
        
        try {
            return new JSONArray(albumsString);
        }
        catch (JSONException e) {
            e.printStackTrace();
            return new JSONArray();
        }
    }
    
    /**
     *
     * @return The last select Album
     */
    public JSONAlbum getSelectedAlbum() {
        return selectedAlbum;
    }
    
    /**
     * Targe a new Album selection and check if this is already a favorite one
     *
     * @param selectedAlbum Album selected for visualization
     */
    public void setSelectedAlbum(JSONAlbum selectedAlbum) {
        JSONArray albumsArray = getAlbumsFileArray();
        
        for (int i = 0; i < albumsArray.length(); i++) {
            try {
                if (albumsArray.getJSONObject(i).getString("name")
                        .equals(selectedAlbum.getName())
                        && albumsArray.getJSONObject(i).getString("artist")
                        .equals(selectedAlbum.getArtist())) {
                    selectedAlbum.setFavorite(true);
                    break;
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        
        this.selectedAlbum = selectedAlbum;
    }
    
    /**
     * Method which call webService for an POST request for Artist Albums
     *
     * @param query Name of the artist to search in the API
     * @param page Number of the search page, 50 results per page
     * @return Send back an LiveData to trigger the listener when ArrayList Generic change.
     */
    public LiveData<ArrayList<?>> getSearchAlbums(String query, int page) {
        Map<String, String> params = new HashMap<>();
        params.put(Parameters.METHOD, Parameters.METHOD_ARTIST_GET_TOPALBUMS);
        params.put(Parameters.ARTIST, query);
        params.put(Parameters.API_KEY, Keys.LAST_FM_API_KEY);
        params.put(Parameters.FORMAT, Parameters.FORMAT_JSON);
        params.put(Parameters.PAGE, Integer.toString(page));
        
        if (page == 0) {
            searchArrayList = new ArrayList<>();
        }
        
        final MutableLiveData<ArrayList<?>> data = new MutableLiveData<>();
        
        webService.post(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i("Repository", "OnResponse URL: " + String.valueOf(call.request().url()));
                
                searchArrayList.addAll(getJSONAlbums(response));
                data.setValue(searchArrayList);
            }
            
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i("Repository", "OnFailure URL: " + String.valueOf(call.request().url()));
                Log.w("Repository", t.getMessage());
            }
        });
        
        return data;
    }
    
    /**
     * Method which call webservice POST request for tracks from an Album
     *
     * @return String in a List format with tracks names
     */
    public LiveData<String> getAlbumTracks() {
        Map<String, String> params = new HashMap<>();
        params.put(Parameters.METHOD, Parameters.METHOD_ALBUM_GET_INFO);
        params.put(Parameters.ARTIST, selectedAlbum.getArtist());
        params.put(Parameters.ALBUM, selectedAlbum.getName());
        params.put(Parameters.API_KEY, Keys.LAST_FM_API_KEY);
        params.put(Parameters.FORMAT, Parameters.FORMAT_JSON);
        
        final MutableLiveData<String> data = new MutableLiveData<>();
        
        webService.post(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i("Repository", "OnResponse URL: " + String.valueOf(call.request().url()));
                data.setValue(getStringTracksList(response));
            }
            
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i("Repository", "OnFailure URL: " + String.valueOf(call.request().url()));
                Log.w("Repository", t.getMessage());
            }
        });
        
        return data;
    }
    
    /**
     * From the Retrofit Callback it generate the JSONAlbum object
     *
     * @param response Response from Retrofit Call
     * @return return an generic ArrayList based on JSONAlbum
     */
    private ArrayList<JSONAlbum> getJSONAlbums(Response<ResponseBody> response) {
        ArrayList<JSONAlbum> tagArrayList = new ArrayList<>();
        
        try {
            JSONObject object = new JSONObject(getStringFromRetrofitResponse(response));
            JSONArray albums = object.getJSONObject("topalbums").optJSONArray("album");
            
            for (int i = 0; i < albums.length(); i++) {
                JSONObject album = albums.optJSONObject(i);
                JSONObject albumObject = new JSONObject();
                
                albumObject.put("name", album.getString("name"));
                albumObject.put("artist", album.getJSONObject("artist").getString("name"));
                albumObject.put("image", album.getJSONArray("image").getJSONObject(3).getString("#text"));
                
                tagArrayList.add(new JSONAlbum(albumObject));
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return tagArrayList;
    }
    
    /**
     * Method used to get and organise tracks from an album
     *
     * @param response Response from Retrofit Call
     * @return Return a String in list format of tracks
     */
    private String getStringTracksList(Response<ResponseBody> response) {
        StringBuilder tracksList = new StringBuilder();
        
        try {
            JSONObject object = new JSONObject(getStringFromRetrofitResponse(response));
            JSONArray tracks =
                    object.getJSONObject("album").optJSONObject("tracks").optJSONArray("track");
            
            for (int i = 0; i < tracks.length(); i++) {
                JSONObject track = tracks.optJSONObject(i);
                
                String name = track.getString("name");
                
                tracksList.append(name).append(System.getProperty("line.separator"));
            }
            
            if (tracks.length() == 0) {
                tracksList.append("No Results");
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        
        return tracksList.toString();
    }
    
    /**
     * Helper that extract the string from response making it easier to read
     *
     * @param response Response directly from Retrofit Call
     * @return One string with the body of a JSON Object
     */
    private static String getStringFromRetrofitResponse(Response<ResponseBody> response) {
        //Try to get response body
        BufferedReader reader;
        StringBuilder sb = new StringBuilder();
        ResponseBody responseBody = response.body();
        if (responseBody != null) {
            try (InputStream inputStream = responseBody.byteStream()) {
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                
                try {
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
