package com.can_apps.data;

import android.content.Context;
import android.content.ContextWrapper;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class InternalStorage extends ContextWrapper {
    
    public InternalStorage(Context base) {
        super(base);
    }
    
    public void write(String filename, byte[] bytes) {
        try {
            FileOutputStream outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(bytes);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public String read(String filename) {
        try {
            FileInputStream inputStream = openFileInput(filename);
        
            StringBuilder fileContent = new StringBuilder();
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(new BufferedInputStream(inputStream)));
    
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                fileContent.append(line);
                fileContent.append('\n');
            }
            bufferedReader.close();
            return fileContent.toString();
        }
        catch (java.io.IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
