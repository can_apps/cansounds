package com.can_apps.data;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface WebService {
    String BASE_URL = "http://ws.audioscrobbler.com/2.0/";
    
    /** Post function in retrofit for API call.
     *
     * @param params Map with key and value for API
     * @return return in Callback for Success or Failure
     */
    @POST("http://ws.audioscrobbler.com/2.0/")
    Call<ResponseBody> post(@QueryMap(encoded = true) Map<String, String> params);
    //encoded = true make " " became %20""
    
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
