package com.can_apps.cansounds.main;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.util.Log;

import com.can_apps.adapters.AlbumListAdapter;
import com.can_apps.adapters.ListAdapter;
import com.can_apps.data.Repository;
import com.can_apps.objects.JSONAlbum;

import java.util.ArrayList;
import java.util.Objects;

// Have to be public so the Fabric can create a instance of it
public class MainViewModel extends ViewModel {
    final static String HOME_SYSTEM_CALL = "HOME_SYSTEM_CALL";
    final static String NEW_PAGE = "NEW_PAGE";
    private int page;
    
    private final MutableLiveData<ListAdapter> adapter = new MutableLiveData<>();
    @SuppressLint("StaticFieldLeak")
    private Repository repository;
    private String lastQuery;
    
    public MainViewModel(Context context) {
        adapter.setValue(new AlbumListAdapter());
        repository = Repository.getInstance(context);
    }
    
    LiveData<ListAdapter> getAdapter() {
        return adapter;
    }
    
    LiveData<ArrayList<?>> getList(String query) {
        LiveData<ArrayList<?>> listLiveData;
        
        switch (query) {
            case HOME_SYSTEM_CALL:
                listLiveData = repository.getSavedAlbums();
                break;
            case NEW_PAGE:
                page++;
                Log.i("Search page", Integer.toString(page));
                listLiveData = repository.getSearchAlbums(lastQuery, page);
                break;
            default:
                lastQuery = query;
                page = 0;
                listLiveData = repository.getSearchAlbums(query, page);
        }
        return listLiveData;
    }
    
    public void updateList(ArrayList<?> objects) {
        Objects.requireNonNull(adapter.getValue()).updateListContent(objects);
    }
    
    public LiveData<Integer> itemClick() {
        return Objects.requireNonNull(adapter.getValue()).getItemClickPosition();
    }
    
    public void selectAlbum(Integer position) {
        repository.setSelectedAlbum(
                (JSONAlbum) Objects.requireNonNull(adapter.getValue()).getAlbum(position));
    }
}
