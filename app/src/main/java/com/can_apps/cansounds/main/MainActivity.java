package com.can_apps.cansounds.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.can_apps.cansounds.R;
import com.can_apps.cansounds.detail.DetailActivity;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    
    private ActionBar actionBar;
    private SwipeRefreshLayout mainSwipeRefresh;
    private MainViewModel viewModel;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    
    private boolean home;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_act);
        Log.i(getString(R.string.MAIN_TAG), getString(R.string.OnCreate));
        
        Toolbar mainToolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(mainToolbar); // Set to act like Action Bar
        actionBar = getSupportActionBar();
        changeMenuName(R.string.favorites);
        
        mainSwipeRefresh = findViewById(R.id.main_swipe_refresh_layout);
        viewModel = new MainViewModel(this);
        recyclerView = findViewById(R.id.main_recycler_view);
        
        viewModel.itemClick().observe(this, position -> {
            viewModel.selectAlbum(position);
            openDetailActivity();
        });
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        Log.i(getString(R.string.MAIN_TAG), getString(R.string.OnStart));
        
        mainSwipeRefresh.setOnRefreshListener(() -> {
            Log.i(getString(R.string.MAIN_TAG), getString(R.string.OnRefresh));
            changeMenuName(R.string.favorites);
            updateList(MainViewModel.HOME_SYSTEM_CALL);
        });
        
        // List settings
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        viewModel.getAdapter().observe(this, listAdapter -> recyclerView.setAdapter(listAdapter));
        
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            boolean isSlidingToLast = false;
            int lastVisibleItemPosition = 0;
            int lastPositions = 0;
            
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE && layoutManager.getItemCount() > 0) {
                    lastPositions = layoutManager.getItemCount();
                    lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                    if (lastPositions - 1 == lastVisibleItemPosition && isSlidingToLast && !home) {
                        updateList(MainViewModel.NEW_PAGE);
                    }
                }
            }
            
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                isSlidingToLast = dy > 0;
            }
        });
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        changeMenuName(R.string.favorites);
        updateList(MainViewModel.HOME_SYSTEM_CALL);
    }
    
    // Menu on Toolbar actions
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tool_bar_menu, menu);
        
        // Get the MenuItem for the action item
        final MenuItem searchItem = menu.findItem(R.id.toolbar_search_btn);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        
        // Show and Hide others menus when search is selected
        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                MainActivity.this.setItemsVisibility(menu, searchItem, false);
                return true;
            }
            
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                MainActivity.this.setItemsVisibility(menu, searchItem, true);
                MainActivity.this.invalidateOptionsMenu();
                return true;
            }
        });
        
        // Search action
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i(getString(R.string.MAIN_TAG), getString(R.string.OnSearchSubmit) + query);
                changeMenuName(R.string.results);
                searchItem.collapseActionView();
                updateList(query);
                return false;
            }
            
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    
    // Toolbar selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_home_btn:
                Log.i(getString(R.string.MAIN_TAG), getString(R.string.OnHomeBtnSelect));
                changeMenuName(R.string.favorites);
                updateList(MainViewModel.HOME_SYSTEM_CALL);
                return true;
            
            case R.id.toolbar_search_btn:
                Log.i(getString(R.string.MAIN_TAG), getString(R.string.OnSearchBtnSelect));
                return true;
            
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void updateList(String query) {
        Log.i(getString(R.string.MAIN_TAG), getString(R.string.OnListUpdate) + query);
        // Signal SwipeRefreshLayout to start the progress indicator
        mainSwipeRefresh.setRefreshing(true);
        
        home = query.equals(MainViewModel.HOME_SYSTEM_CALL);
        
        viewModel.getList(query).observe(this, objects -> {
            updateUI();
            viewModel.updateList(objects);
        });
        
    }
    
    private void updateUI() {
        Log.i(getString(R.string.MAIN_TAG), getString(R.string.OnUiUpdate));
        // Signal SwipeRefreshLayout to end the progress indicator
        if (mainSwipeRefresh != null && mainSwipeRefresh.isRefreshing()) {
            mainSwipeRefresh.setRefreshing(false);
        }
    }
    
    private void openDetailActivity() {
        Intent intent = new Intent(this, DetailActivity.class);
        startActivity(intent);
    }
    
    private void changeMenuName(int name) {
        Objects.requireNonNull(actionBar).setTitle(name);
    }
    
    private void setItemsVisibility(Menu menu, MenuItem exception, boolean visible) {
        for (int i = 0; i < menu.size(); ++i) {
            MenuItem item = menu.getItem(i);
            if (item != exception) item.setVisible(visible);
        }
    }
}