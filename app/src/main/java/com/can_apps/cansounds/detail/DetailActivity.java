package com.can_apps.cansounds.detail;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.can_apps.cansounds.R;
import com.can_apps.objects.JSONAlbum;

import java.util.Objects;

public class DetailActivity extends AppCompatActivity {
    private ActionBar actionBar;
    private DetailViewModel viewModel;
    private ImageView detailImage;
    private TextView detailName;
    private TextView detailArtist;
    private TextView detailTracks;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
    
        Toolbar mainToolbar = findViewById(R.id.detail_toolbar);
        setSupportActionBar(mainToolbar); // Set to act like Action Bar
        actionBar = getSupportActionBar();
        viewModel = new DetailViewModel(this);
        detailImage = findViewById(R.id.detail_image);
        detailName = findViewById(R.id.detail_first_text);
        detailArtist = findViewById(R.id.detail_second_text);
        detailTracks = findViewById(R.id.detail_tracks_text);
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        Objects.requireNonNull(actionBar).setTitle(R.string.details);
        actionBar.setHomeButtonEnabled(true);
        
        JSONAlbum album = viewModel.getAlbum();
        detailName.setText(album.getName());
        detailArtist.setText(album.getArtist());
    
        // Image
        Glide.with(getApplicationContext())
                .load(album.getImage())
                .placeholder(R.drawable.uni)
                .error(R.drawable.uni)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .into(detailImage);
    
        if (album.getTracks().isEmpty()) {
            viewModel.getTracks().observe(this, tracks -> {
                viewModel.getAlbum().setTracks(tracks);
                detailTracks.setText(tracks);
            });
        }
        else {
            detailTracks.setText(album.getTracks());
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.detail_toolbar_menu, menu);
        
        if (viewModel.getAlbum().isFavorite()){
            menu.getItem(0).setIcon(R.drawable.ic_favorite_24dp);
        }
        else {
            menu.getItem(0).setIcon(R.drawable.ic_favorite_border_24dp);
        }
        
        return super.onCreateOptionsMenu(menu);
    }
    
    // Toolbar selection
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.toolbar_favorite_btn:
                if (viewModel.getAlbum().isFavorite()){
                    item.setIcon(R.drawable.ic_favorite_border_24dp);
                    albumSavedMessage(viewModel.saveAlbum(false));
                    viewModel.getAlbum().setFavorite(false);
                }
                else {
                    item.setIcon(R.drawable.ic_favorite_24dp);
                    albumSavedMessage(viewModel.saveAlbum(true));
                    viewModel.getAlbum().setFavorite(true);
                }
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void albumSavedMessage(Boolean saved) {
        String message = getString(R.string.album_removed);
        if (saved) message = getString(R.string.album_saved);
    
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}
