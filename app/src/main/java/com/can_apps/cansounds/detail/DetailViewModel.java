package com.can_apps.cansounds.detail;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.can_apps.data.Repository;
import com.can_apps.objects.JSONAlbum;

public class DetailViewModel extends ViewModel {
    @SuppressLint("StaticFieldLeak")
    private Repository repository;
    
    public DetailViewModel(Context context) {
        repository = Repository.getInstance(context);
    }
    
    public boolean saveAlbum(boolean save) {
        return repository.saveAlbum(repository.getSelectedAlbum(), save);
    }
    
    public JSONAlbum getAlbum() {
        return repository.getSelectedAlbum();
    }
    
    public LiveData<String> getTracks() {
        return repository.getAlbumTracks();
    }
}
