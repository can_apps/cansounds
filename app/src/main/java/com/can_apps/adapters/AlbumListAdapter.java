package com.can_apps.adapters;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.can_apps.cansounds.R;
import com.can_apps.objects.JSONAlbum;

import org.json.JSONObject;

import java.util.ArrayList;

public class AlbumListAdapter extends ListAdapter {
    private ArrayList<JSONAlbum> albumArrayList = new ArrayList<>();
    
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_card, parent, false);
        return new AlbumListAdapter.ViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.firstText.setText(albumArrayList.get(position).getName());
        viewHolder.secondText.setText(albumArrayList.get(position).getArtist());
        
        // Image
        Glide.with(viewHolder.itemView.getContext())
                .load(albumArrayList.get(position).getImage())
                .placeholder(R.drawable.uni)
                .error(R.drawable.uni)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .into(viewHolder.backgroundImg);
    }
    
    @Override
    public int getItemCount() {
        return albumArrayList.size();
    }
    
    @Override
    public void updateListContent(ArrayList<?> arrayList) {
        albumArrayList = (ArrayList<JSONAlbum>) arrayList;
        notifyDataSetChanged();
    }
    
    @Override
    public JSONObject getAlbum(Integer position) {
        return albumArrayList.get(position);
    }
}
