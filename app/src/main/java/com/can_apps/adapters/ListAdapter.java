package com.can_apps.adapters;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.can_apps.cansounds.R;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Abstract class used as parent for Adapters, making it easier to use same functions with
 * generic ArrayLists.
 */
public abstract class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private final MutableLiveData<Integer> position = new MutableLiveData<>();
    
    public abstract void updateListContent(ArrayList<?> arrayList);
    
    public abstract JSONObject getAlbum(Integer position);
    
    protected void onItemClick(int adapterPosition) {
        position.setValue(adapterPosition);
    }
    
    public LiveData<Integer> getItemClickPosition() {
        return position;
    }
    
    class ViewHolder extends RecyclerView.ViewHolder {
        
        ImageView backgroundImg;
        TextView firstText;
        TextView secondText;
        
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            
            backgroundImg = itemView.findViewById(R.id.list_background);
            firstText = itemView.findViewById(R.id.list_first_text);
            secondText = itemView.findViewById(R.id.list_second_text);
            
            itemView.setOnClickListener(view -> onItemClick(getAdapterPosition()));
        }
    }
}
