# CanSounds by vcanato@gmail.com

# Resume
Project create as portfolio using based on the API Call with Last.fm APIs

This project implements Retrofit, MVVM architecture, Internal Storage, RecyclerView lists
 with adapters and more that can be checked on CHANGELOG file.
 
 # To run
 
 ## Keys
 To run this project is need to add an `Keys` java public class under the package `com.can_apps`.
 ### Last.fm
 Last.fm API_key can be obtained on "https://www.last.fm/api" and should be avaiable under the 
 variable `LAST_FM_API_KEY` inside `keys.java`